const mng = require("mongoose");
const orderSchema = new mng.Schema({
	customer: {
		type: String,
		trim: true,
		immutable: true,
		required: [true, "Customer name is required for order"]
	},
	ordIndx: {
		type: Number,
		unique: true,
		immutable: true,
		required: [true, "Order index is required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total amount of orders is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date(),
		immutable: true
	},
	productOrders: [{
		productId: {
			type: String,
			required: [true, "Product ID for order is required"]
		},
		price: {
			type: Number,
			required: [true, "Product price for order is required"]
		},
		ordNum: {
			type: Number,
			required: [true, "Amount of orders is required"]
		}
	}]
});
const ORDER = mng.model("Order", orderSchema);
module.exports = ORDER;
