const mng = require("mongoose");
const cartSchema = new mng.Schema({
		productId: String,
		price: Number,
		ordNum: Number
	});
const userSchema = new mng.Schema({
	email: {
		type: String,
		trim: true,
		immutable: true,
		required: [true, "Email is required"],
		unique: [true, "This email is already taken"]
	},
	password: {
		type: String,
		required: [true, "Password is required"],
		minLength:[8, "Password should at least be 8 charactrs long"] 
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date(),
		immutable: true
	},
	orderHistory: {
		type: [{orderId	: String}]
	},
	cart: { 
		type: [{productId: String, price: Number, ordNum: Number}],

	}
});
const USER = mng.model("User", userSchema);
module.exports = USER;