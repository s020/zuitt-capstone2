const mng = require("mongoose");
const indxSchema = new mng.Schema({
	name: {
		type: String,
		required: [true, "Please specify index category"]
	},
	lastIndx: {
		type: Number,
		default: 0
	}
});

const INDEX = new mng.model("Indices", indxSchema);
module.exports = INDEX;