const mng = require("mongoose");
const productSchema = new mng.Schema({
	name: {
		type: String,
		trim: true,
		required: [true, "Product name is required"]
	},
	nameLower: {
		type: String,
		trim: true,
		required: [true, "Lowercase name is needed for searching"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	prodIndx: {
		type: Number,
		unique: true,
		immutable: true,
		required: [true, "Product index is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
	stock: {
		type: Number,
		required: [true, "Stock amount is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date(),
		immutable: true
	},

	img: {
		link: {
			type: String
		},
		style: {
			type: String
		}
	}
});
const PRODUCT = mng.model("Product", productSchema);
module.exports = PRODUCT;