const exp = require("express");
const route = exp.Router();
const controller = require("../controllers/users.js");
const login = require("../controllers/login.js");

module.exports = route;

route.get('/getAll', login.adminOnly, (req, res) => { 
	controller.getAllUsers().then(result => res.send(result));
});

route.get('/get/:id', login.adminOnly, (req, res) => { 
	controller.getUserById(req.params.id).then(result => res.send(result));
});

route.get('/getUser/:id', login.userOnly, (req, res) => {
	controller.getUserById(req.params.id).then(result => res.send(result));
})

route.post('/register', (req, res) => {
	controller.createUser(req.body).then(result => res.send(result));
});

route.delete('/delete/:id', login.adminOnly, (req, res) => {
	controller.deleteUserById(req.params.id).then(result => res.send(result));
});

route.put('/update/self', login.isAdmin2, (req, res) => {
	controller.updateByUser(req, res).then(result => res.send(result));
});

route.put('/update/:id', login.adminOnly, (req,res) => {
	controller.updateByAdmin(req, res).then(result => res.send(result));
});

route.put('/setAdmin', login.adminOnly, (req, res) => {
	if (req.user.isAdmin)
		controller.setAdmin({email: req.body.email}, {isAdmin: req.body.isAdmin}).then(result => res.send(result));
	else 
		res.status(400).json({error: "Unauthorized action"});
});