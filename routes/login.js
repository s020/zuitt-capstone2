const exp = require("express");
const route = exp.Router();
const controller = require("../controllers/login");

route.post("/", (req, res) => {
	let mail = req.body.email;
	let pass = req.body.password;
	let reqBody = req.body;
	if(mail != "" && pass != "" && mail != undefined && pass != undefined){
		controller.login(mail, pass, reqBody).then((result) => {
			if(result == true) {
				res.json({token: 
					controller.mkTkn({email: mail, isAdmin: reqBody.isAdmin}),
					email: mail,
					isAdmin: reqBody.isAdmin,
					fullName: reqBody.fullName,
					id: reqBody.id
				});
			}
			else
				res.status(400).json({error: result});
		});
	}
	else res.status(400).json({error: "Please provide input for all fields"});	
});

module.exports = route;

route.get("/adminOnly", controller.adminOnly, (req, res) => { res.json({message: "success"}) });
route.get("/userOnly", controller.userOnly, (req, res) => { res.json({message: "success"}) });
