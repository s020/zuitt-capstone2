const exp = require("express");
const route = exp.Router();
const controller = require("../controllers/products.js");
const login = require("../controllers/login.js");

module.exports = route;

route.get('/getAllActive', (req, res) => {
	controller.getAllActive().then(result => res.send(result));
});

route.get('/getAll', (req, res) => {
	controller.getAllProducts().then(result => res.send(result));
});

route.get('/get-filtered', (req, res) => {
	controller.getProducts(req.body).then(result => res.send(result));
});

route.get('/get-word', (req, res) => {
	controller.getProductsByWord(req.body).then(result => res.send(result));
})

route.get('/get/:id', (req, res) => {
	controller.getProductById(req.params.id).then(result => res.send(result));
});

route.post('/create', login.adminOnly, (req, res) => {
	controller.createProduct(req.body).then(result => res.send(result));
});

route.delete('/delete/:id', login.adminOnly, (req, res) => {
	controller.deleteProductById(req.params.id).then(result => res.send(result));
});

route.put('/update/:id', login.adminOnly, (req,res) => {
	controller.updateProduct(req.params.id, req.body).then(result => res.send(result));
});

route.put('/update/archive/:id', login.adminOnly, (req,res) => {
	controller.archive(req.params.id, {isActive: req.body.isActive}).then(result => res.send(result));
});

route.put('/updateNewFields', login.adminOnly,async (req,res) => {
	res.send(await controller.updateMany(req.body));
})