const exp = require("express");
const route = exp.Router();
const controller = require("../controllers/orders.js");
const login = require("../controllers/login.js");

route.get('/getAll', login.isAdmin2, (req, res) => { //isAdmin for privilege-specific get
	controller.getAllOrders(req).then(result => res.send(result));
});

route.get('/get/ind/:index', login.isAdmin2, (req, res) => {
	controller.getOrderByIndex(req).then(result => res.send(result));
});

route.get('/get/:id', login.isAdmin2, (req, res) => { //isAdmin for privilege-specific get
	controller.getOrderById(req).then(result => res.send(result));
});

route.post('/create', login.userOnly, (req, res) => {
	controller.createOrder(req).then(result => res.send(result));
});

route.delete('/delete/:id', login.adminOnly, (req, res) => {
	controller.deleteOrderById(req.body).then(result => res.send(result));
});

route.get('/getCart', login.userOnly, (req, res) => {
	controller.getCart(req).then(result => res.send(result));
});		

route.put('/updateCart', login.userOnly, (req, res) => {
	controller.updateCart(req).then(result => res.send(result));
});

route.put('/update/:id', login.adminOnly, (req,res) => {
	controller.updateOrder(req.params.id, req.body).then(result => res.send(result));
});

module.exports = route;


