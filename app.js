// CONST Declarations
	const exp = require("express");
	const mng = require("mongoose");
	const env = require("dotenv").config();
	const app = exp();
	const pnv = process.env;
	const ORT = require("./routes/orders.js");
	const PRT = require("./routes/products.js");
	const URT = require("./routes/users.js");
	const LGN = require("./routes/login.js");
	const CRS = require("cors");

// App Use functions
	app.use(CRS())
	app.use(exp.json());
	app.use("/users", URT);
	app.use("/products", PRT);
	app.use("/orders", ORT);
	app.use("/login", LGN);


// Server and Database setup
	
	mng.connect(pnv.DB_CONNECT_STRING);
	
	app.listen(pnv.PORT, (req, res) => {
		console.log(`Server is connected at PORT ${pnv.PORT}`);
	});

	mng.connection.once('open', (req, res) => {
		console.log(`Server is now connected to the database`);
	});

	app.get('/', (req, res) => {
		res.send("Welcome to Enrico Miguel H. Silvano's App!");
	})

