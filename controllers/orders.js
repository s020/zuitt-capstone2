const Product = require("../models/Product");
const Order = require("../models/Orders");
const Index = require("../models/Indices");
const User = require("../models/User.js");
const func = require("./myFunctions");

module.exports.createOrder = (req) => { //this is the checkout/finalize order
	let total = 0;
	let mail = req.user.email;
	return User.findOne({email: mail}).then(async(result) => {
		if (result.cart.length == 0) 
			return {error: "Cart is empty"};

		result.cart.forEach(x => {
			total += (x.price * x.ordNum);
			
			Product.findByIdAndUpdate( (x.productId) , { $inc: { "stock": -x.ordNum}}).then(ok => {
				console.log(ok)
			});
		});

		let newOrder = new Order({
			customer: mail,
			ordIndx: await func.indexer(Index, "orders"),
			totalAmount: total,
			productOrders: result.cart
		});
		return newOrder.save().then((ok) => {
			if (ok) {
				result.orderHistory.push({orderId: ok.ordIndx});
				result.cart = [];
				result.save();
				return {message: "Order confirmed"};
			}
			else 
				return {error: "There has been an error taking your order"};
		}).catch(err => {
			return {error: "There has been an error taking your order"};
		});
	});
};
 
module.exports.getAllOrders = (req) => {
	if(req.user.isAdmin)
		return func.find(Order, {});
	else 
		return func.find(Order, {customer: req.user.email});
};

module.exports.getOrderById = (req) => {
	return func.getById(Order, req.params.id, "Order");
};

module.exports.getOrderByIndex = (req) => {
	if(req.user.isAdmin)
		return func.getByIndex(Order, {ordIndx: req.params.index});
	else {
		return User.findOne({email: req.user.email, "orderHistory.orderId" : req.params.index}).then(result => {
			if (result == null)
				return {error: "Order not found"};
			return func.getByIndex(Order, {ordIndx: req.params.index});
		});
	}
};


module.exports.deleteOrderById = (id) => {
	return func.delete(Order, id, "Order");
};

module.exports.updateOrder = (id, data) => {
	return func.update(Order, id, data, "Order");
};

module.exports.updateCart = async (req) => { //this also works as an update and remove from cart
	let toAdd = req.body.productId;
	let num = req.body.orderNum;
	let message = "";
	let price1 = 0;
	let stock = 0;
	if(!(await Product.findOne({ _id: toAdd}).then(ok => {
		stock = ok.stock;
		price1 = ok.price;
		return ok.isActive;
	})))	
		return new Promise((resolve, reject) => resolve({error: "Product not available"}));

	if (stock < num) {
			return new Promise((resolve, reject) => resolve({error: `Product stock is insufficient`}));
	}

	return User.findOne({email: req.user.email}).then(result => {

		let addItem = () => {
			result.cart.push({productId: toAdd, price: price1, ordNum: num});	
			result.save();
			return {message: "Item added to cart"};
		};

		if(result.cart.length > 0){	
			result.cart.forEach(x => {
				if (x.productId == toAdd) {
					if (x.ordNum + num > 0) {
						x.ordNum += num;
						result.save();
						message = {message: "Cart successfully updated"};
						return;	
					}
					else if(x.ordNum + num == 0) {
						result.cart.pull(x);
						result.save();
						message = {message: "Item removed from Cart"};
						return;
					}
					else {
						message = {message: "Invalid request"};
						return;
					}
				}
			});
			if (message == "") return addItem(); //if no duplicate item is found
			else return message;
		}
		else if (num > 0) return addItem();
		else return {error: "Invalid request"};
	}).catch(e => {return {error: "Invalid request"}});
};


module.exports.getCart = (req) => {
	return User.findOne({email: req.user.email}).then(result => {
		if (result.cart.length == 0)
			return {message: "Cart is empty. Fill up your cart now!"}
		return result.cart;
	});
};