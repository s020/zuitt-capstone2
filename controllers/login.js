const exp = require("express");
const bcr = require("bcrypt");
const jwt = require("jsonwebtoken");
const env = require("dotenv").config();
const User = require("../models/User");
const pnv = process.env;

module.exports.mkTkn = (data) => {
	return jwt.sign(data, pnv.TOKEN_SECRET, {expiresIn: '6h'});
};

module.exports.chkTkn = (req, res, next) => { 
	if (req.headers.authorization == undefined) {
		res.json({error: "Invalid Credentials"});
		return false;
	}
	const authHead = req.headers.authorization;
	const tkn = authHead.split(" ");
	if(tkn[0] != "Bearer") {
		res.json({error: "Invalid Credentials"});
		return false;
	}
	try {
		req.user = jwt.verify(tkn[1], pnv.TOKEN_SECRET);
		return true;
	}
	catch {
		res.json({error: "Invalid Credentials"});	
		return false;
	}
};

module.exports.login = (mail, pass, reqBody) => {
	return User.findOne({email: mail}).then(result => {
		if (result != null) {
			return bcr.compare(pass, result.password).then(valid => {
					if (valid){
						reqBody.isAdmin = result.isAdmin;
						reqBody.fullName = result.fName + " " + result.lName;
						reqBody.id = result._id;
						return true;
					}
					else 
						return {error: "Invalid Password"};
			});
		}
				else 
			return {error: "Invalid Email"};
	});
};

module.exports.isAdmin2 = (req, res, next) => {
	module.exports.isAdmin(req, res, next);
	next();
}

module.exports.isAdmin = (req, res, next) => { //no next method -- non-middleware
	if (!module.exports.chkTkn(req, res, next)) {
	}
	if(req.user.isAdmin != true)
		return false;
	else
		return true;
};

module.exports.adminOnly = (req, res, next) => {
	if (!module.exports.isAdmin(req, res, next)) {
		res.json({error: "Forbidden Action"});
		return;
 	}
	next();
};

module.exports.userOnly = (req, res, next) => {
	if (module.exports.isAdmin(req, res, next)) {
		res.json({error: "Forbidden Action - Customers only"});
		return;
	}
	next();
};