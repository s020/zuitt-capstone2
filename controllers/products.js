const Product = require("../models/Product");
const func = require("./myFunctions");
const Index = require("../models/Indices");

module.exports.createProduct = async (data) => {
	if(data.prodName !== "" && data.prodDescription !== "" && data.prodPrice !== "") {
		let newProduct = new Product({
			name: data.name,
			nameLower: data.name.toLowerCase(),
			description: data.description,
			prodIndx: await func.indexer(Index, "products"),
			price: data.price,
			stock: data.stock,
			img: data.img
		});
		return newProduct.save().then(ok => {if (ok) return {message: "Product saved successfully"};}).catch(err => {return {error: "Failed to save product"};});
	}
	else
		return {error: "Please provide input for all fields"};
};

module.exports.getAllActive = () => {
	return func.find(Product, {isActive: true});
}

module.exports.getAllProducts = () => {
	return func.find(Product, {});
};

module.exports.getProductById = (id) => {
	return func.getById(Product, id, "Product")
};

module.exports.getProductsByWord = (data) => {
	return func.getByWord(Product, data);
};

module.exports.deleteProductById = (id) => {
	return func.delete(Product, id, "Product");
};

module.exports.updateProduct = (id, data) => {
	return func.update(Product, id, data, "Product")
};

module.exports.getProducts = (filter) => {
	return func.find(Product, filter);
};

module.exports.archive = (id, data) => {
	return func.archive(Product, id, data, "Product");
};

module.exports.updateMany = (data) => {
	return Product.updateMany({}, data);
}