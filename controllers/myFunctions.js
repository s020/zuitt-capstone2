const exp = require("express");
const mng = require("mongoose");
const route = exp.Router();

//I realized I could lessen my code

module.exports.find = (a, toFind) => { return a.find(toFind).then(result =>
{ if (result.length == 0) return {message: "No match found"}; return result; }); };

module.exports.getById = (a, id, type) => {
	return a.findById(id).then(ok => {
		if (ok == null)
			return {message: `${type} not found`};
		else
			return ok;
	}).catch(err => {
		return{message: `${type} not found`};
	});
};

module.exports.getByWord = (a, data) => {
	let b = Object.keys(data)[0];
	return a.find({[b]: {$regex: data[b], $options: 'i'}, isActive: true}).then(ok => { 
		if(ok.length == 0)
			return {message: "No match found"}
		return ok });
}

module.exports.delete = (a, id, type) => {
	return a.findByIdAndDelete(id).then((ok, err) => {
		if (ok)
			return {message: `${type} successfully deleted`};
		else
			return {message: `Failed to delete ${type}`};
	});
};

module.exports.update = (a, id, data, type) => {
	return a.findByIdAndUpdate(id, data).then((ok, err) => {
		if (ok)
			return {message: `${type} successfully updated`};
		else
			return {message: `Failed to update ${type}`};
	});
};

module.exports.updateOne = (a, toFind, data, type) => {
	return a.findOneAndUpdate(toFind, data).then(ok => {
		if (ok){
			console.log(ok)
			return {message: `${type} successfully updated`};}
		else
			return {message: `Failed to update ${type}`};
	});
};


module.exports.archive = (a, id, data, type) => {
	return a.findByIdAndUpdate(id, data).then(ok => {
		if (ok && data.isActive == false) {
			return {message: `${type} successfully archived`};
		}
		else
			return {message: `${type} removed from/not in archive`};
	});
}


module.exports.indexer = async (a, xName) => {
	return await a.findOne({name: xName}).then(res => {
		res.lastIndx += 1;
		res.save();
		return res.lastIndx;
	});
}

module.exports.getByIndex = (a, index) => {
	return a.findOne(index).then(result => {
		return result;
	});
}