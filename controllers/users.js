const User = require("../models/User");
const func = require("./myFunctions");
const bcrypt = require("bcrypt");
const env = require("dotenv").config();
const pnv = process.env;

let passHash = (data) => {
	return bcrypt.hashSync(data, parseInt(pnv.SALT));
};

let passChk = (data) => {
	if (data != undefined){
		if (data.length >= 8) 
			return 1;
		else if (data.length < 8) 
			return 2;
	}
	else 
		return 3;
}
module.exports.createUser = (data) => {
	var newUser = new User();

	console.log(data)
	console.log(typeof data)
	if(data.email != "" && data.password != "" && data.password != undefined) {
		if (data.password.length < 8)
			return new Promise((resolve, reject) => { resolve({error: "Password is too short"})});
		newUser.email = data.email.toLowerCase(),
		newUser.password = passHash(data.password)
	}

	newUser.fName = data.fName;
	newUser.lName = data.lName;
	newUser.phone = data.phone;
	newUser.bday  = data.bday;
	newUser.address = data.address;
	return newUser.save().then(ok => {
		if(ok)
			return {message: "User successfully saved."};
	}).catch(err => {
		if(err.code === 11000)
			return {error: "Email already in use"};
		else
			return {error: "Please provide input for all fields"};
	});
};

module.exports.getAllUsers = () => {
	return func.find(User, {});
};

module.exports.getUserById = (id) => {
	return func.getById(User, id, "User");
};

module.exports.deleteUserById = (id) => {
	return func.delete(User, id, "User");
};

module.exports.updateByAdmin = (req, res) => {
	let p = req.body.password;
	let chk = passChk(p);
	if(chk == 1) {
 		req.body.password = passHash(p);
	}
	else if (chk == 2) {
		res.status(400).send({error: "Password length must be at least 8 characters"});
		return;
	}
	return func.update(User, req.params.id, req.body, "User");
};

module.exports.updateByUser = (req, res) => {
	switch (passChk(req.body.password)) {
		case 1:
			req.body.password = passHash(req.body.password);
			break;
		case 2:
			return new Promise((resolve, reject) => resolve({error: "Password length must be at least 8 characters"}));
			break;		
	}

	return func.updateOne(User, {email: req.user.email}, req.body, "User");
};

module.exports.setAdmin = (email, data) => {
	return func.updateOne(User, email, data, "Admin privileges");
}